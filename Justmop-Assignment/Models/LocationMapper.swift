//
//  LocationMapper.swift
//  Justmop-Assignment
//
//  Created by Luai Kalkatawi on 10.07.2018.
//  Copyright © 2018 Luai Kalkatawi. All rights reserved.
//

import UIKit

class LocationMapper: Codable {
    var area_name: String?
    var city_name_english: String?
    var polygoncenter: String?
}
