//
//  DataModel.swift
//  Justmop-Assignment
//
//  Created by Luai Kalkatawi on 10.07.2018.
//  Copyright © 2018 Luai Kalkatawi. All rights reserved.
//

import UIKit
import CoreLocation

struct CleanerItem {
    var name: String
    var assignedLocation: CLLocation
}

struct LocationsQueue <CLLocation> {
    var list = [CLLocation]()
    
    mutating func enqueue(_ element: CLLocation) {
        list.append(element)
    }
    
    mutating func dequeue() -> CLLocation? {
        if !list.isEmpty {
            let randomIndex = Int(arc4random_uniform(UInt32(list.count)))
            return list.remove(at: randomIndex)
        } else {
            return nil
        }
    }
}
