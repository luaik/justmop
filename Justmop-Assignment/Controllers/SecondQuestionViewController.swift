//
//  SecondQuestionViewController.swift
//  Justmop-Assignment
//
//  Created by Luai Kalkatawi on 10.07.2018.
//  Copyright © 2018 Luai Kalkatawi. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

class SecondQuestionViewController: UIViewController {
    @IBOutlet weak var notificationActionLabel: UILabel!
    
    var locationManager = CLLocationManager()
    var cleanerArray: [CleanerItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCleanersAndLocations()
        initLocationManager()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [[.alert, .sound, .badge]], completionHandler: { (granted, error) in
            if (granted) {
                //self.locationManager.startUpdatingLocation()
            } else {
                // Do Something
            }
        })
        UNUserNotificationCenter.current().delegate = self
    }
    
    func setCleanersAndLocations() {
        var locationList = LocationsQueue<CLLocation>()
        locationList.enqueue(CLLocation(latitude: 37.78583400, longitude: -122.40641700)) //Writtern for simulator test
        //locationList.enqueue(LocationItem(latittude:41.006243, longtitude: 29.037883))
        locationList.enqueue(CLLocation(latitude: 41.001485, longitude: 29.041083))
        locationList.enqueue(CLLocation(latitude: 40.995153, longitude: 29.042693))
        cleanerArray = [CleanerItem(name: "Sarah", assignedLocation: locationList.dequeue()!),
                        CleanerItem(name: "Rose", assignedLocation: locationList.dequeue()!),
                        CleanerItem(name: "Sandra", assignedLocation: locationList.dequeue()!)]
    }
    
    func sendNotification(cleaner:CleanerItem) {
        let content = UNMutableNotificationContent()
        content.title = "Cleaner Found"
        content.subtitle = ""
        content.body = cleaner.name + " is in this location."
        content.categoryIdentifier = "demoNotificationCategory"
        
        let choiceA = UNNotificationAction(identifier: "cleanerPicked", title: "Cleaner picked", options: [.foreground])
        let choiceB = UNNotificationAction(identifier: "cleanerNotPicked", title: "Cleaner not picked", options: [.foreground])
        let categories = UNNotificationCategory(identifier: "demoNotificationCategory", actions: [choiceA, choiceB], intentIdentifiers: [], options: [])
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        let requestIdentifier = "demoNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().setNotificationCategories([categories])
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
        })
    }
    
    func findCleanerInLocation(driverLocation:CLLocation?) {
        if (driverLocation != nil) {
            let closestCleaner = cleanerArray.filter{$0.assignedLocation.distance(from: driverLocation!) < 50}.first
            if closestCleaner != nil {
                locationManager.stopUpdatingLocation()
                sendNotification(cleaner: closestCleaner!)
            }
        }
    }
}

extension SecondQuestionViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        findCleanerInLocation(driverLocation: locations.first)
    }
}

extension SecondQuestionViewController: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
        if response.actionIdentifier == "cleanerPicked" {
            print("CLEANER PICKED")
            self.notificationActionLabel.text = "CLEANER PICKED"
        }else if response.actionIdentifier == "cleanerNotPicked" {
            print("CLEANER NOT PICKED")
            self.notificationActionLabel.text = "NOT CLEANER PICKED"
        }
    }
}
