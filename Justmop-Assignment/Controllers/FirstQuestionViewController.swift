//
//  FirstQuestionViewController.swift
//  Justmop-Assignment
//
//  Created by Luai Kalkatawi on 10.07.2018.
//  Copyright © 2018 Luai Kalkatawi. All rights reserved.
//

import UIKit

class FirstQuestionViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    
    let aArray: [Int] = [1, 3, 4, 5]
    let bArray: [Int] = [-1, 3, 0, 9]
    let cArray: [Int] = [0, 31, 32, 22, 6]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        find2ElementsAtleastPresentIn2Arrays(a: aArray, b: bArray, c: cArray)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func find2ElementsAtleastPresentIn2Arrays(a aList: [Int], b bList: [Int], c cList: [Int]) {
        let allArray = aList + bList + cList
        let result = Array(Set(allArray.filter({ (i: Int) in allArray.filter({$0 == i}).count > 1 })))
        resultLabel.text = result.description
    }
}
