//
//  ThirdQuestionViewController.swift
//  Justmop-Assignment
//
//  Created by Luai Kalkatawi on 10.07.2018.
//  Copyright © 2018 Luai Kalkatawi. All rights reserved.
//

import UIKit
import CoreLocation

class ThirdQuestionViewController: UIViewController {
    @IBOutlet weak var locationLatLongLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var regionTextField: UITextField!
    
    var list: [LocationMapper] = []
    var latitude = 0.0
    var longtitude = 0.0
    var cities: [String] = []
    var regions: [String] = []
    var selectedRegionPolygoncenter = ""
    var citiesPickerView = UIPickerView()
    var regionPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareDataSource()
        preparePickerView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func preparePickerView() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePicker(_:)))
        toolBar.setItems([doneButton], animated: false)

        citiesPickerView.delegate = self
        regionPickerView.delegate = self
        cityTextField.inputAccessoryView = toolBar
        regionTextField.inputAccessoryView = toolBar
        cityTextField.inputView = citiesPickerView
        regionTextField.inputView = regionPickerView
    }
    
    func prepareDataSource() {
        if let path = Bundle.main.path(forResource: "data", ofType: "txt") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: [])
                let resultObj = try JSONDecoder().decode(DataResponse.self, from: jsonData)
                
                if let sheet = resultObj.Sheet1 {
                    list = sheet
                    cities.append(contentsOf: list.map{$0.city_name_english ?? ""})
                    cities = Array(NSOrderedSet(array: cities)) as! [String]
                }
                
            } catch let error {
                print("error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid path.")
        }
    }
    
    func formatPolygonCenter(polygoncenter: String) {
        let latLongValue = polygoncenter.components(separatedBy: ",")
        
        if latLongValue.count == 2 {
            latitude = Double(latLongValue[0].trimmingCharacters(in: .whitespaces).prefix(9))!
            longtitude = Double(latLongValue[1].trimmingCharacters(in: .whitespaces).prefix(9))!
        }
        
        locationLatLongLabel.text = "( \(latitude) , \(longtitude) )"
    }
    
    @objc func donePicker(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    @IBAction func getRandomLocationActionButton(_ sender: UIButton) {
        regions.removeAll()
        self.view.endEditing(true)
        cityTextField.text = ""
        regionTextField.text = ""

        let randomValue = arc4random_uniform(UInt32(list.count))
        
        if let center = list[Int(randomValue)].polygoncenter {
            formatPolygonCenter(polygoncenter: center)
        }
    }
    
    @IBAction func FindMyCityRegionActionButton(_ sender: UIButton) {
        let selectedLocation = list.filter{$0.polygoncenter!.contains(latitude.description) && $0.polygoncenter!.contains(longtitude.description)}.first
        if (selectedLocation != nil){
            cityTextField.text = selectedLocation?.city_name_english
            regionTextField.text = selectedLocation?.area_name
        }
    }
}

extension ThirdQuestionViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return cities.count
        }else{
            return regions.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == citiesPickerView {
            return cities[row]
        }else{
            return regions[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == citiesPickerView {
            cityTextField.text = cities[row]
        }else{
            regionTextField.text = regions[row]
            
            if let city = cityTextField.text {
                selectedRegionPolygoncenter = ((list.filter{($0.city_name_english?.contains(city))! && ($0.area_name?.contains(regions[row]))!}).map{$0.polygoncenter}.first as? String)!
                formatPolygonCenter(polygoncenter: selectedRegionPolygoncenter)
            }
        }
    }
}

extension ThirdQuestionViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == cityTextField {
            regions.removeAll()
            cityTextField.text = ""
            regionTextField.text = ""
            locationLatLongLabel.text = ""
            self.citiesPickerView.selectRow(0, inComponent: 0, animated: true)
            self.pickerView(citiesPickerView, didSelectRow: 0, inComponent: 0)
        }else if textField == regionTextField, cityTextField.text?.count == 0 {
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, textField.tag == 0 {
            regions = (list.filter{($0.city_name_english?.contains(text))!}).map{$0.area_name!}
        }
    }
}
